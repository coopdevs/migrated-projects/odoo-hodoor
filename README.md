### DEVELOPMENT

#### Create development enviornment

Create the `devenv` container with the `hodoor` module mounted and provision it. Follow the [instructions](https://gitlab.com/coopdevs/odoo-hodoor-inventory#instructions) in [odoo-hodoor-inventory](https://gitlab.com/coopdevs/odoo-hodoor-inventory).

Once created, we can stop or start our `odoo-hodoor` lxc container as indicated here:
```sh
$ sudo systemctl start lxc@odoo-hodoor
$ sudo systemctl stop lxc@odoo-hodoor
```

To check our local lxc containers and their status, run:
```sh
$ sudo lxc-ls -f
```

#### Start the ODOO application

Enter to your local machine as the user `odoo`, activate the python enviornment first and run the odoo bin:
```sh
$ ssh odoo@odoo-hodoor.local
$ pyenv activate odoo
$ cd /opt/odoo
$ ./odoo-bin -c /etc/odoo/odoo.conf -u hodoor -d odoo
```

#### Restart ODOO database from scratch

Enter to your local machine as the user `odoo`, activate the python enviornment first, drop the DB, and run the odoo bin to create it again:
```sh
$ ssh odoo@odoo-hodoor.local
$ pyenv activate odoo
$ dropdb odoo
$ cd /opt/odoo
$ ./odoo-bin -c /etc/odoo/odoo.conf -i hodoor -d odoo --stop-after-init
```

#### Run tests

You can run the tests with this command:
```sh
$ ./odoo-bin -c /etc/odoo/odoo.conf -u hodoor -d odoo --stop-after-init --test-enable
```

The company data is rewritten every module upgrade

Credits
=======

###### Authors

* Coopdevs Treball SCCL

###### Contributors

* Coopdevs Treball SCCL
